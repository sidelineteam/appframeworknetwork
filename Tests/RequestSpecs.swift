//
//  RequestSpecs.swift
//  AppFrameworkNetwork
//
//  Created by Jorge Miguel on 19/02/2025.
//

import XCTest
@testable import AppFrameworkNetwork

class RequestTests: XCTestCase {
    // MARK: - Properties
    
    var sut: Request<DataRequestType>!
    let testURL = URL(string: "https://api.server.com")!
    
    // MARK: - Setup/Teardown
    
    override func setUp() {
        super.setUp()
        sut = DataRequest(url: testURL)
    }
    
    override func tearDown() {
        sut = nil
        super.tearDown()
    }
    
    // MARK: - Configuration Tests
    
    func testDefaultConfiguration() {
        // Given
        let config = sut.configurations
        
        // Then
        XCTAssertEqual(config.method, .auto)
        XCTAssertTrue(config.useCache)
        XCTAssertTrue(config.allowCellUsage)
        XCTAssertEqual(config.timeoutInterval, 60)
        XCTAssertNil(config.headers)
        XCTAssertNil(config.body)
        XCTAssertNil(config.bodyData)
    }
    
    func testCustomConfiguration() {
        // Given
        let customConfig = RequestConfig()
        customConfig.method = .post
        customConfig.useCache = false
        customConfig.allowCellUsage = false
        customConfig.timeoutInterval = 30
        customConfig.headers = ["Authorization": "Bearer token"]
        customConfig.body = "test body"
        
        // When
        sut = DataRequest(url: testURL, config: customConfig)
        
        // Then
        XCTAssertEqual(sut.configurations.method, .post)
        XCTAssertFalse(sut.configurations.useCache)
        XCTAssertFalse(sut.configurations.allowCellUsage)
        XCTAssertEqual(sut.configurations.timeoutInterval, 30)
        XCTAssertEqual(sut.configurations.headers?["Authorization"], "Bearer token")
        XCTAssertEqual(sut.configurations.body, "test body")
    }
    
    // MARK: - Error Handling Tests
    
    func testHandleErrorNoInternet() {
        // Given
        let error = NSError(domain: NSURLErrorDomain, code: NSURLErrorNotConnectedToInternet, userInfo: nil)
        
        // When
        let networkError = DataRequest.handleError(error)
        
        // Then
        XCTAssertEqual(networkError, NetworkError.noInternetConnection)
    }
    
    func testHandleStatusCodeErrors() {
        // Given
        let testCases = [
            (400, NetworkError.badRequest),
            (401, NetworkError.unauthorized),
            (403, NetworkError.forbidden),
            (404, NetworkError.notFound),
            (500, NetworkError.internalServerError)
        ]
        
        // Then
        for (statusCode, expectedError) in testCases {
            XCTAssertEqual(sut.handleStatusCode(statusCode), expectedError)
        }
    }
    
    // MARK: - Request Creation Tests
    
    func testCreateRequestWithDefaults() {
        // When
        let request = sut.createRequest()
        
        // Then
        XCTAssertEqual(request.url, testURL)
        XCTAssertTrue(request.allowsCellularAccess)
        XCTAssertEqual(request.timeoutInterval, 60)
        XCTAssertEqual(request.allHTTPHeaderFields, [:])
        XCTAssertEqual(request.httpMethod, "GET")
    }
    
    func testCreateRequestWithCustomHeaders() {
        
        // Given
        let headers = ["Content-Type": "application/json"]
        sut = DataRequest(url: testURL, config: DataRequest.config { $0.headers = headers })
        
        // When
        let request = sut.createRequest()
        
        // Then
        XCTAssertEqual(request.allHTTPHeaderFields?["Content-Type"], "application/json")
    }
    
    // MARK: - Content Type Tests
    
    func testContentTypeForDifferentRequestTypes() {
        // Given
        let xmlRequest = XMLRequest(url: testURL)
        let jsonRequest = JSONRequest(url: testURL)
        let stringRequest = StringRequest(url: testURL)
        let imageRequest = ImageRequest(url: testURL)
        
        // Then
        XCTAssertEqual(xmlRequest.contentType, .xml)
        XCTAssertEqual(jsonRequest.contentType, .json)
        XCTAssertEqual(stringRequest.contentType, .text)
        XCTAssertEqual(imageRequest.contentType, .image)
    }
}
