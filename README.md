# AppFrameworkV2
AppFrameworkV2 is a framework for those who want to start coding their ideas as soon as possible, without having to care about the repetitive code that is required when creating a new project. It is a Swift, more modern version of the AppFramework, written in ObjC.

## How to Get Started

Create the app project including AppFrameworkV2 using `Swift Package Manager`.

Since this framework has a lot of features, sometimes including only a subset might be handy. With that in mind, AppFrameworkV2 is devided in feature targets, each one providing smaller sets of features.

## Installation

### Swift Package Manager

`Swift Package Manager` is the official package manager from Apple. In order to use 'AppFrameworkV2' through it, use the provided tools on XCode. The packages available are:

```swift
AppFrameworkCore
AppFrameworkIAP                 #-- includes AppFrameworkCore
AppFrameworkMocker              #-- includes AppFrameworkCore
AppFrameworkNetwork             #-- includes AppFrameworkCore
AppFrameworkDataStore           #-- includes AppFrameworkCore
```

### Tests

AppFrameworkV2 tests are available on the SPM package.

## Usage

```Swift
// swift-tools-version:5.3

//-- swift build -Xswiftc "-sdk" -Xswiftc "`xcrun --sdk iphonesimulator --show-sdk-path`" -Xswiftc "-target" -Xswiftc "x86_64-apple-ios13.0-simulator"

import PackageDescription

let package = Package(name: "MyApp",
    platforms: [.iOS(.v11), .macOS(.v10_12)],

    products: [
        .library(name: "MyApp", targets: ["MyApp"]),
        .library(name: "MyAppStubs", targets: ["MyAppStubs"])
    ],

    dependencies: [
        //-- AppFramework
        .package(name: "AppFrameworkDataStore", path: "../../appframeworkv2/datastore"),
        .package(name: "AppFrameworkNetwork", path: "../../appframeworkv2/network"),
        .package(name: "AppFrameworkMocker", path: "../../appframeworkv2/mocker"),
        .package(name: "AppFrameworkCore", path: "../../appframeworkv2/core"),
        .package(name: "AppFrameworkIAP", path: "../../appframeworkv2/iap"),
    ],

    targets: [
        .target(name: "MyApp", dependencies: ["AppFrameworkDataStore", "AppFrameworkNetwork"], path: "Sources"),
        .target(name: "MyAppStubs", dependencies: ["MyApp", "AppFrameworkMocker"], path: "Stubs"),

        .testTarget(name: "MyAppTests", dependencies: ["MyAppStubs"], path: "Tests")
    ],

    swiftLanguageVersions: [.v5]
)
```

### AFNetwork (AFNetworkExtension)

#### Architecture

* `Network`
* `NetworkSessions`
* `Request<T>`
  - `DataRequest`
  - `ImageRequest`
  - `JSONRequest`
  - `StringRequest`
  - `XMLRequest`
* `WebSocket`

##### Network

Network provides an easy way to perform network calls, without having to manage queues and other details.

###### Setup

```xml
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
<dict>
	<key>appframework</key>
	<dict>
		<key>network</key>
		<dict>
			<key>max_concurrent_requests_per_host</key>
			<integer>1</integer>
		</dict>
	</dict>
</dict>
</plist>
```

On `statics.plist` (under `appframework.network`):
- Set the `max_concurrent_requests_per_host`, the default maximum number of concurrent calls that Network can perform, per queue.

###### Methods & Vars

```Swift
public static var isOnWifi: Bool = false
public static var isOnCell: Bool = false
public static var isActive: Bool = false
```
Provide information on the current status of the network connection.

```Swift
public static var enableNetworkIndicator: Bool = true
```

If true, the network indicator will be presented when there is network activity.

```Swift
public static func showNetworkActivity(_ show: Bool)
```
Presents/hides the network activity indicator manually. To be used if the app is doing a network call outside of this API.

```Swift
public var sessionProtocolClasses: [AnyClass]?
```

Assigns protocol classes to the session being used.

```Swift
public static func setupQueue(_ name: String, maxConcurrency: Int = 1)
```
Setup the indicated queue manually. Usage is optional.

```Swift
public static func add<T>(request: Request<T>, toQueue queue: String? = nil, priority: Operation.QueuePriority = .normal)
```

Adds a `Request` to the indicated queue.

```Swift
public static func requests(on queue: String) -> [Request<Any>]
```

Returns the list of `Request` that are assigned to the indicated queue.

```Swift
public static func cancelRequest(_ identifier: String)
public static func cancelRequests(on queue: String)
public static func cancelAllRequests()
```

Methods to cancel scheduled requests.

```Swift
public static func suspendAllQueues()
public static func suspendRequests(on queue: String)
```
Methods to suspend queues.

```Swift
public static func resumeAllQueues()
public static func resumeRequests(on queue: String)
```
Methods to resume queues.

```Swift
public static func resetAllCache()
```
Resets the network cache.

```Swift
public static func onIdle(_ handler: @escaping IdleHandler)
```
Executes the handler once all the queues are idle.

##### NetworkSessions

NetworkSessions provides the configured sessions for network calls.

###### Vars

```Swift
public static var defaultConfiguration: URLSessionConfiguration
public static var backgroundConfiguration: URLSessionConfiguration
```
Return the session configured for the desired scenario.

##### Request

Request represents a request task that can be used to do network work.

###### Architecture

The `Request` types available are:

* `DataRequest`: Expects a Data response.
* `ImageRequest`: Expects an UIImage response. Images won't be kept in cache.
* `JSONRequest`: Expects a JSON response and parses it accordingly.
* `StringRequest`: Expects a String response.
* `XMLRequest`: Expects a tuple containing a XMLParser and the XML string as response.

###### Methods & Vars

```Swift
public let identifier: String
```

The id of the request. Can be used to cancel the request when on a queue.

```Swift
public var allowCellUsage: Bool = true
```

If true, the request will be able to be executed over cell network.

```Swift
public let url: URL
```

The url being called by the request.

```Swift
public let body: Data?
```

The `Data` representation of the body being sent by the request.

```Swift
public let headers: [String:String]?
```

The headers being sent by the request.

```Swift
public init(identifier: String = NSUUID().uuidString, url: URL, method: Request.Method = .auto, headers: [String: String]? = nil, body: String? = nil, bodyData: Data? = nil, useCache: Bool = true, authentication: Request.AuthenticationHandler? = nil, progress: Request.ProgressHandler? = nil, success: Request.SuccessHandler<T>? = nil, failure: Request.FailureHandler? = nil, debug: Request.DebugHandler? = nil)
```

* Instantiates a `Request` that connects to `url`, with the id `identifier`.
* The request will be of the type `method` and contain the `headers` and `body` / `bodyData`.
* If `useCache` is true, it will search the cache for a valid response and use it.
* The `authentication` handler can be used to provide the necessary info for requests that require authentication.
* The `progress` handler can be used to monitor the progress of the request activity.
* The `success` and `failure` handlers will be called when the request finishes its job.
* The `debug` handler will provide all the info related to the request, both from the request and response stages. This handler is called only on debug mode.

##### How to use

```Swift
guard let url = URL(string: "http://ip-api.com/json") else { return }

//-- create the request
let request = JSONRequest(url: url, success: { (json, _) in
    print("ISP name: \(json.first?["isp"] ?? "")")
})

//-- add request to the queue
Network.add(request: request)
```
A call to a JSON endpoint can be done as easily as seen above. The success block will return the JSON object already parsed, as well as the original response from the network.

```Swift
guard let url = URL(string: "http://ip-api.com/json") else { return }

//-- perform a JSON call
request = JSONRequest(url: url,
                   method: .post,
                  headers: ["Content-Type": "text/html; charset=utf-8"],
                     body: "<html><body>Just an example</body></html>",
                 bodyData: data_object,	//-- this should be used instead of body. if both are used, body will be used.
                 useCache: false,
    authenticationHandler: { ... },
                 progress: { (requestId, percent) in
    //-- handle progress info
    print("progress: \(Int(percent*100))%")

}, success: { (json, response) in
    //-- handle json response
    print("ISP name: \(json.first?["isp"] ?? "")")

}) { (error, _) in
    //-- handle error
    print("error: \(error.localizedDescription)")
}

//-- add request to the queue
Network.add(request: request)
```
It is also possible to implement the call as seen above, for scenarios that require further configuration.

##### WebSocket

WebSocket represents a websocket task that can be used to stabilish a connection and trade messages between two endpoints.

###### Methods

```Swift
public init(url: URL, status: WebSocket.StatusHandler? = nil, receiver: WebSocket.ReceiverHandler? = nil, debug: WebSocket.DebugHandler? = nil)
```

Instantiates a `WebSocket` that connects to `url`, gets notified of the connection status on the `status` handler and gets received messages on the `receiver` handler. The `debug` handler will provide all the info related to the recieved messages. This handler is called only on debug mode.

```Swift
public func start()
```

Starts / resumes a connection.

```Swift
public func pause()
```

Suspends the connection temporarily.

```Swift
public func stop()
```

Terminates the connection.

```Swift
public func reset()
```

Stops the current connection and recreates the websocket.

```Swift
public func send(_ message: Data, completion: @escaping WebSocket.SenderHandler)
public func send(_ message: String, completion: @escaping WebSocket.SenderHandler)
```

Sends a message.

---

## Dependencies

On this version of AppFramework I choose to use 3rd party libs as much as possible... and justifiable. The libs listed below are the core of some of the AppFrameworkV2's features.

```
FirebaseRemoteConfig #-- Remote configurations  (Core)

Umbrella             #-- Analytics              (Core)

Umbrella/Firebase    #-- Analytics              (Core)

SwiftyStoreKit       #-- InAppPurchases         (IAP)

Realm                #-- Storage                (DataStore)
```

Adding to these, some other libs where also used on AppFrameworkV2 to provide internal features to the AppFrameworkV2, hence not having been wrapped by AppFrameworkV2.

```
DeviceKit         #-- Device info                                (Core)

ReachabilitySwift #-- Network Reachability                       (Network)
```

---

## Author

Jorge Miguel

## License

AppFrameworkV2 is available under the MIT license. See the LICENSE file for more info.
