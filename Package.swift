// swift-tools-version:5.3

//-- swift build -Xswiftc "-sdk" -Xswiftc "`xcrun --sdk iphonesimulator --show-sdk-path`" -Xswiftc "-target" -Xswiftc "x86_64-apple-ios13.0-simulator"
//-- swift build -Xswiftc "-sdk" -Xswiftc "`xcrun --sdk iphonesimulator --show-sdk-path`" -Xswiftc "-target" -Xswiftc "x86_64-apple-macos10.14"

import PackageDescription

let package = Package(name: "AppFrameworkNetwork",

    platforms: [.iOS(.v13), .macOS(.v10_12), .watchOS(.v7)], //, .tvOS(.v10)],

    products: [
        .library(name: "AppFrameworkNetwork", targets: ["AppFrameworkNetwork"])
    ],

    dependencies: [

        //.package(name: "AppFrameworkCore", path: "../core"),
        .package(name: "AppFrameworkCore", url: "https://bitbucket.org/sidelineteam/appframeworkcore", .branch("main")),

        //-- Network
        .package(name: "Reachability", url: "https://github.com/ashleymills/Reachability.swift", from: "5.0.0")
    ],

    targets: [
        .target(name: "AppFrameworkNetwork",
                dependencies: ["AppFrameworkCore", .product(name: "Reachability", package: "Reachability", condition: .when(platforms: [.iOS, .macOS]))],
                path: "Sources"),

        //-- test target
        .testTarget(name: "AppFrameworkNetworkTests", dependencies: ["AppFrameworkNetwork"], path: "Tests")
    ],

    swiftLanguageVersions: [.v5]
)
