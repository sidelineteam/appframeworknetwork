//
//  NetworkStaticConfigs.swift
//  AppFrameworkV2
//
//  Created by Jorge Miguel on 17/03/2020.
//  Copyright (c) 2020 Jorge Miguel. All rights reserved.
//

import AppFrameworkCore

extension StaticConfigs {

    internal struct Network {
        
        private static let kMaxConcurrentRequestsPerHost = StaticConfig<Int>("appframework.network.max_concurrent_requests_per_host", defaultValue: 1)

        internal static var maxConcurrentRequestsPerHost: Int { Configurations.get(kMaxConcurrentRequestsPerHost) }
    }
}
