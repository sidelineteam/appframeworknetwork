//
//  NetworkSessions.swift
//  AppFrameworkV2
//
//  Created by Jorge Miguel on 09/11/2018.
//  Copyright (c) 2018 Jorge Miguel. All rights reserved.
//

import Foundation

public class NetworkSessions {

    public static var backgroundConfiguration: URLSessionConfiguration = {
        
        //-- fix cache folder permissions
        NetworkSessions.fixPermissions()

        return NetworkSessionConfigs.background
    }()

    public static var defaultConfiguration: URLSessionConfiguration = {
        NetworkSessionConfigs.default
    }()

    private static func fixPermissions() {
        if let plist = Bundle.main.path(forResource: "Info", ofType: "plist"),
            let dict = NSDictionary(contentsOfFile: plist) as? [String: AnyObject],
            var path = NSSearchPathForDirectoriesInDomains(.libraryDirectory, .userDomainMask, true).first,
            let bundle = dict["CFBundleIdentifier"] {
            path.append("/Caches/com.apple.nsurlsessiond/Downloads/\(bundle)")

            if FileManager.default.fileExists(atPath: path) {
                try? FileManager.default.setAttributes([FileAttributeKey.protectionKey: FileProtectionType.completeUntilFirstUserAuthentication], ofItemAtPath: path)
            }
        }
    }

}

fileprivate class NetworkSessionConfigs {

    fileprivate static var background: URLSessionConfiguration = {
        let config = URLSessionConfiguration.background(withIdentifier: "\(Bundle.main.bundleIdentifier ?? "this.app.session").background")
        config.httpCookieAcceptPolicy = .onlyFromMainDocumentDomain
        config.httpShouldSetCookies = false
        config.httpShouldUsePipelining = false
        config.httpMaximumConnectionsPerHost = 1
        config.isDiscretionary = true
        config.networkServiceType = .background
        config.requestCachePolicy = .reloadIgnoringCacheData
        config.sessionSendsLaunchEvents = false
        config.shouldUseExtendedBackgroundIdleMode = false
        config.waitsForConnectivity = true
        
        return config
    }()

    fileprivate static let `default` = URLSessionConfiguration.default
}
