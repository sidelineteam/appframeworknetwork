//
//  Request.swift
//  AppFramework
//
//  Created by Jorge Miguel on 10/07/2018.
//  Copyright (c) 2018 Jorge Miguel. All rights reserved.
//

//*********************************************************************
// -network-debug launch argument can be used to enable the debug mode
//*********************************************************************

import UIKit
import Foundation
import AppFrameworkCore

public typealias CodableRequestType  = Decodable
public typealias DataRequestType     = Data
public typealias DownloadRequestType = URL
public typealias ImageRequestType    = UIImage
public typealias XMLRequestType      = (parser: XMLParser, xml: String)
public typealias JSONRequestType     = [[String:Any]]
public typealias StringRequestType   = String

public typealias DecodableRequest = Request<CodableRequestType>
public typealias DataRequest      = Request<DataRequestType>
public typealias DownloadRequest  = Request<DownloadRequestType>
public typealias ImageRequest     = Request<ImageRequestType>
public typealias JSONRequest      = Request<JSONRequestType>
public typealias StringRequest    = Request<StringRequestType>
public typealias XMLRequest       = Request<XMLRequestType>

public typealias CodableRequestSuccessHandler  = DecodableRequest.SuccessHandler<CodableRequestType>
public typealias DataRequestSuccessHandler     = DataRequest.SuccessHandler<DataRequestType>
public typealias DownloadRequestSuccessHandler = DownloadRequest.SuccessHandler<DownloadRequestType>
public typealias ImageRequestSuccessHandler    = ImageRequest.SuccessHandler<ImageRequestType>
public typealias XMLRequestSuccessHandler      = XMLRequest.SuccessHandler<XMLRequestType>
public typealias JSONRequestSuccessHandler     = JSONRequest.SuccessHandler<JSONRequestType>
public typealias StringRequestSuccessHandler   = StringRequest.SuccessHandler<StringRequestType> //-- add other String parsers...

public class RequestConfig {
    
    public enum Method : String {
        case auto   = "auto"
        case delete = "delete"
        case get    = "get"
        case post   = "post"
        case put    = "put"
        case upload = "upload"
        case download = "download"
    }
    
    public var identifier: String = NSUUID().uuidString
    public var method: Method = .auto
    public var headers: [String: String]?
    public var body: String?
    public var bodyData: Data?
    public var useCache: Bool = true
    public var allowCellUsage: Bool = true
    public var timeoutInterval: Double = 60
    
    //-- StringRequest
    public var responseBodyEncoding: String.Encoding = .utf8
    
    public init() { }
}

extension Request {
    
    public typealias ConfigHandler = (_ configs: RequestConfig) -> Void
    
    //--

    public enum Headers : String {
        case contentType = "Content-Type"
        case contentLength = "Content-Length"
    }

    public enum ContentType : String {
        case codable   = "application/json"
        case data      = "application/octet-stream"
        case image     = "image/jpeg"
        case json      = "application/json; charset=utf-8"
        case text      = "text/plain"
        case xml       = "text/xml"
    }
    
    public struct DebugInfo {
        public let url: URL?
        public let body: Data?
        public let headers: [String:Any]?
    }

    public typealias AuthenticationHandler = () -> [String: String]?
    public typealias ProgressHandler       = (_ requestId: String, _ transfered: Int64, _ total: Int64) -> Void
    public typealias SuccessHandler<Value> = (_ value: Value, _ response: HTTPURLResponse?) -> Void
    public typealias FailureHandler        = (_ error: NetworkError, _ response: HTTPURLResponse?) -> Void
    public typealias DebugHandler          = (_ request: DebugInfo, _ response: DebugInfo?) -> Void
    public typealias TaskHandler           = (Data?, URLResponse?, Error?) -> Void
    
    //--
    
    public static func config(_ configs: ConfigHandler) -> RequestConfig {
        let options = RequestConfig()
        configs(options)
        return options
    }
}

open class CodableRequest<T: Decodable, V: Encodable> : Request<T> {
    
    private let jsonEncoder: JSONEncoder = {
        let encoder = JSONEncoder()
        encoder.dateEncodingStrategy = .iso8601
        encoder.keyEncodingStrategy = .useDefaultKeys
        return encoder
    }()
    
    private let jsonDecoder: JSONDecoder = {
        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .iso8601
        decoder.keyDecodingStrategy = .useDefaultKeys
        return decoder
    }()
    
    public init(url: URL, body: V? = nil, config: RequestConfig = RequestConfig(), authentication: Request<T>.AuthenticationHandler? = nil, progress: Request<T>.ProgressHandler? = nil, success: Request<T>.SuccessHandler<T>? = nil, failure: Request<T>.FailureHandler? = nil, debug: Request<T>.DebugHandler? = nil) {
        
        if let bodyData = body {
            do {
                config.bodyData = try jsonEncoder.encode(bodyData)
                
            } catch {
                Log.error("CodableRequest()", "JSON encoding failed: \(error)")
            }
        }
        
        super.init(url: url, config: config, authentication: authentication, progress: progress, success: success, failure: failure, debug: debug)
    }
}

open class Request<T> : Operation, URLSessionDataDelegate, URLSessionDownloadDelegate {
    
    public let url: URL
    public let configurations: RequestConfig
    
    internal lazy var contentType: Request.ContentType = {
        switch T.self {
        case is XMLRequestType.Type: return .xml
        case is JSONRequestType.Type: return .json
        case is StringRequestType.Type: return .text
        case is ImageRequestType.Type: return .image
        default: return .data
        }
    }()
        
    fileprivate lazy var cachePolicy: NSURLRequest.CachePolicy = {
        self.configurations.useCache || self._successHandler == nil ? .returnCacheDataElseLoad : .reloadIgnoringLocalCacheData
    }()
    
    private lazy var body: Data? = {
        if let bodyStr = self.configurations.body {
            return bodyStr.data(using: .utf8)
            
        } else {
            return self.configurations.bodyData
        }
    }()
    
    private lazy var method: RequestConfig.Method = {
        if self.configurations.method == .auto {
            return (self.body != nil ? .post : .get)
            
        } else {
            return self.configurations.method
        }
    }()
    
    fileprivate var task: URLSessionTask?
    
    public  var authenticationHandler: Request.AuthenticationHandler?
    private let progressHandler: Request.ProgressHandler?
    public  var debugHandler: Request.DebugHandler?
    private var taskHandler: Request.TaskHandler?
    
    private(set) var totalTransefered: Int64 = 0
    private(set) var totalTransferSize: Int64 = 0
    
    //-- request handles success on main thread
    private let _successHandler: Request.SuccessHandler<T>?
    fileprivate lazy var successHandler: Request.SuccessHandler<T> = { (value, response) in
        DispatchQueue.main.async {
            self._successHandler?(value, response)
        }
    }
    
    //-- request handles failure on main thread
    private let _failureHandler: Request.FailureHandler?
    fileprivate lazy var failureHandler: Request.FailureHandler = { (error, response) in
        DispatchQueue.main.async {
            self._failureHandler?(error, response)
        }
    }
    
    private lazy var session: URLSession = {
#if os(watchOS)
        let config = NetworkSessions.defaultConfiguration
        
#else
        var state: UIApplication.State = .active
        
#if !kExtensionTarget
        DispatchQueue.main.sync {
            //state = UIApplication.shared.applicationState
        }
#endif
        
        let config = (state == .background ? NetworkSessions.backgroundConfiguration : NetworkSessions.defaultConfiguration)
#endif
        
        if config.protocolClasses?.count ?? 0 == 0 && Network.shared.sessionProtocolClasses?.count ?? 0 > 0 {
            config.protocolClasses = Network.shared.sessionProtocolClasses
        }
        
        config.timeoutIntervalForRequest = self.configurations.timeoutInterval
        config.timeoutIntervalForResource = self.configurations.timeoutInterval
        
        return URLSession(configuration: config, delegate: self, delegateQueue: nil)
    }()
    
    //--
    
    public init(url: URL, config: RequestConfig = RequestConfig(), authentication: Request<T>.AuthenticationHandler? = nil, progress: Request<T>.ProgressHandler? = nil, success: Request<T>.SuccessHandler<T>? = nil, failure: Request<T>.FailureHandler? = nil, debug: Request<T>.DebugHandler? = nil) {
            
        self.url = url
        self.configurations = config
        
        self.authenticationHandler = authentication
        self.progressHandler = progress
        self.debugHandler = debug
        
        self._successHandler = success
        self._failureHandler = failure
    }
    
    @available(*, deprecated, message: "Use init(url:config:authentication:progress:success:failure:debug:) instead")
    public init(identifier: String = NSUUID().uuidString, url: URL, method: RequestConfig.Method = .auto, headers: [String: String]? = nil, body: String? = nil, bodyData: Data? = nil, useCache: Bool = true, authentication: Request.AuthenticationHandler? = nil, progress: Request.ProgressHandler? = nil, success: Request.SuccessHandler<T>? = nil, failure: Request.FailureHandler? = nil, debug: Request.DebugHandler? = nil) {
        
        self.url = url
        self.configurations = Request.config {
            $0.identifier = identifier
            $0.method = method
            $0.headers = headers
            $0.body = body
            $0.bodyData = bodyData
            $0.useCache = useCache
        }
        
        self.authenticationHandler = authentication
        self.progressHandler = progress
        self.debugHandler = debug
        
        self._successHandler = success
        self._failureHandler = failure
    }
    
    //-- MARK: execution
    
    open override func main() {
//        guard Network.isOnWifi || (self.allowCellUsage && Network.isOnCell) else {
//            Log.debug(className, "No internet available. Discarding request!")
//            self.failureHandler(.noInternetConnection, nil)
//            return
//        }
        
        if self._successHandler != nil {
            runTask()
            
        } else {
            dataTask()
        }
    }
    
    //-- MARK: download (with completion)
    
    fileprivate func runTask() {
        
        //-- make operation wait for the end of the network call
        let group = DispatchGroup()
        group.enter()
        
        self.taskHandler = { (data, response, error) in
            group.leave()
            
            Network.showNetworkActivity(false)
            
            guard error == nil else {
                let error = Request.handleError(error!)
                self.failureHandler(error, nil)
                
                //-- record for stub
                self.storeRequestInfo()
                
                return
            }
            
            //-- empty body response (http code == 204)
            if let httpResponse = response as? HTTPURLResponse, httpResponse.statusCode == 204 {
                switch self.contentType {
                case .xml:
                    if let handler = self.successHandler as? XMLRequestSuccessHandler {
                        handler((XMLParser(), ""), httpResponse)
                        
                    } else {
                        assert(false, "\(ClassInfo.methodName(self)): Well... this shouldn't happen")
                        self.failureHandler(NetworkError.wrongOrMissingCallaback, response as? HTTPURLResponse)
                    }
                    
                case .json:
                    if let handler = self.successHandler as? JSONRequestSuccessHandler {
                        handler([], httpResponse)
                        
                    } else {
                        assert(false, "\(ClassInfo.methodName(self)): Well... this shouldn't happen")
                        self.failureHandler(NetworkError.wrongOrMissingCallaback, response as? HTTPURLResponse)
                    }
                    
                case .text:
                    if let handler = self.successHandler as? StringRequestSuccessHandler {
                        handler("", httpResponse)
                        
                    } else {
                        assert(false, "\(ClassInfo.methodName(self)): Well... this shouldn't happen")
                        self.failureHandler(NetworkError.wrongOrMissingCallaback, response as? HTTPURLResponse)
                    }
                    
                case .image:
                    if let handler = self.successHandler as? ImageRequestSuccessHandler {
                        handler(UIImage(), httpResponse)
                        
                    } else {
                        assert(false, "\(ClassInfo.methodName(self)): Well... this shouldn't happen")
                        self.failureHandler(NetworkError.wrongOrMissingCallaback, response as? HTTPURLResponse)
                    }
                    
                default:
                    if let handler = self.successHandler as? DataRequestSuccessHandler {
                        handler(Data(), httpResponse)
                        
                    } else {
                        assert(false, "\(ClassInfo.methodName(self)): Well... this shouldn't happen")
                        self.failureHandler(NetworkError.wrongOrMissingCallaback, response as? HTTPURLResponse)
                    }
                }
                
                return
            }
            
            //-- not success and not redirect
            let successRange = 200...209
            if let httpResponse = response as? HTTPURLResponse,
               successRange.contains(httpResponse.statusCode) == false && httpResponse.statusCode != 308 {
                let error = self.handleStatusCode(httpResponse.statusCode)
                self.failureHandler(error, response as? HTTPURLResponse)
                return
            }
            
            guard let data = data else {
                if let handler = self.successHandler as? DownloadRequestSuccessHandler {
                    handler(self.downloadUrl!, response as? HTTPURLResponse)
                    return
                }
                
                assert(false, "\(ClassInfo.methodName(self)): Well... this shouldn't happen (to improve...)")
                self.failureHandler(NetworkError.badResponse, response as? HTTPURLResponse)
                return
            }
            
            //-- cache response
            if let response = response, data.isEmpty == false, self.cachePolicy == .returnCacheDataElseLoad {
                let cacheResponse = CachedURLResponse(response: response, data: data)
                URLCache.shared.storeCachedResponse(cacheResponse, for: self.task as! URLSessionDataTask)
            }
            
            //-- response handling
            switch self.contentType {
            case .xml:
                if let handler = self.successHandler as? XMLRequestSuccessHandler, let xml = data.utf8String {
                    let xmlParser = XMLParser(data: data)
                    handler((xmlParser, xml), response as? HTTPURLResponse)
                    
                } else {
                    assert(false, "\(ClassInfo.methodName(self)): Well... this shouldn't happen")
                    self.failureHandler(NetworkError.wrongOrMissingCallaback, response as? HTTPURLResponse)
                }
                
                //-- record for stub
                self.storeResponseInfo(response, responseBody: data)
                
            case .json:
                if let handler = self.successHandler as? JSONRequestSuccessHandler {
                    let json = try? JSONSerialization.jsonObject(with: data)
                    if let json = json as? [[String:Any]] {
                        handler(json, response as? HTTPURLResponse)
                        
                    } else if let json = json as? [String:Any] {
                        handler([json], response as? HTTPURLResponse)
                        
                    } else {
                        self.failureHandler(NetworkError.badResponse, response as? HTTPURLResponse)
                    }
                    
                } else {
                    assert(false, "\(ClassInfo.methodName(self)): Well... this shouldn't happen")
                    self.failureHandler(NetworkError.wrongOrMissingCallaback, response as? HTTPURLResponse)
                }
                
                //-- record for stub
                self.storeResponseInfo(response, responseBody: data)
                
            case .text:
                if let handler = self.successHandler as? StringRequestSuccessHandler {
                    guard let str = String(data: data, encoding: self.configurations.responseBodyEncoding) else {
                        assert(false, "\(ClassInfo.methodName(self)): Well... this shouldn't happen (to improve...)")
                        self.failureHandler(NetworkError.badResponse, response as? HTTPURLResponse)
                        return
                    }
                    
                    handler(str, response as? HTTPURLResponse)
                    
                } else {
                    assert(false, "\(ClassInfo.methodName(self)): Well... this shouldn't happen")
                    self.failureHandler(NetworkError.wrongOrMissingCallaback, response as? HTTPURLResponse)
                }
                
                //-- record for stub
                self.storeResponseInfo(response, responseBody: data)
                
            case .image:
                if let handler = self.successHandler as? ImageRequestSuccessHandler {
                    guard let image = UIImage(data: data) else {
                        assert(false, "\(ClassInfo.methodName(self)): Well... this shouldn't happen (to improve...)")
                        self.failureHandler(NetworkError.badResponse, response as? HTTPURLResponse)
                        return
                    }
                    
                    handler(image, response as? HTTPURLResponse)
                    
                } else {
                    assert(false, "\(ClassInfo.methodName(self)): Well... this shouldn't happen")
                    self.failureHandler(NetworkError.wrongOrMissingCallaback, response as? HTTPURLResponse)
                }
                
                //-- record for stub
                self.storeResponseInfo(response, responseBody: nil)
                
            case .codable:
                if let handler = self.successHandler as? CodableRequestSuccessHandler {
                    do {
                        guard let D = T.self as? Decodable.Type else {
                            return
                        }
                        
                        let decodedResponse = try JSONDecoder().decode(D, from: data)
                        handler(decodedResponse, response as? HTTPURLResponse)
                    } catch {
                        assert(false, "\(ClassInfo.methodName(self)): Well... this shouldn't happen decoding")
                        self.failureHandler(NetworkError.badResponse, response as? HTTPURLResponse)
                    }
                    
                } else {
                    assert(false, "\(ClassInfo.methodName(self)): Well... this shouldn't happen")
                    self.failureHandler(NetworkError.wrongOrMissingCallaback, response as? HTTPURLResponse)
                }
                
            default:
                if let handler = self.successHandler as? DataRequestSuccessHandler {
                    handler(data, response as? HTTPURLResponse)
                    
                } else {
                    assert(false, "\(ClassInfo.methodName(self)): Well... this shouldn't happen")
                    self.failureHandler(NetworkError.wrongOrMissingCallaback, response as? HTTPURLResponse)
                }
                
                //-- record for stub
                self.storeResponseInfo(response, responseBody: data)
            }
        }
        
        guard let handler = self.taskHandler, let task = self.createTask(completion: handler) else {
            self.failureHandler(NetworkError.cancelled, nil)
            return
        }
        
        if isCancelled {
            self.failureHandler(NetworkError.cancelled, nil)
            Network.showNetworkActivity(false)
            return
        }
        
        //-- is it cached?
        if self.cachePolicy == .returnCacheDataElseLoad,
           let task = task as? URLSessionDataTask, let request = task.currentRequest,
           let response = URLCache.shared.cachedResponse(for: request),
           response.data.isEmpty == false {
            
            handler(response.data, response.response, nil)
            return
        }
        
        Network.showNetworkActivity(true)
        
        task.resume()
        
        group.wait()
    }
    
    //-- MARK: download (without completion)
    
    private func dataTask() {
        
        //-- make operation wait for the end of the network call
        let group = DispatchGroup()
        group.enter()
        
        let handler: Request.TaskHandler = { (data, response, error) in
            group.leave()
            
            Network.showNetworkActivity(false)
        }
        
        guard let task = self.createTask(completion: handler) else {
            self.failureHandler(NetworkError.cancelled, nil)
            return
        }
        
        if isCancelled {
            self.failureHandler(NetworkError.cancelled, nil)
            Network.showNetworkActivity(false)
            return
        }
        
        Network.showNetworkActivity(true)
        
        task.resume()
        
        //-- record for stub
        self.storeRequestInfo()
        
        group.wait()
    }
    
    //-- MARK: cancel
    
    open override func cancel() {
        self.task?.cancel()
        super.cancel()
    }
    
    //-- MARK: request creation (private helper)
    
    internal func createRequest() -> URLRequest {
        var _headers = self.configurations.headers ?? [:]
        
        //-- add authentication info
        if let authHeaders = self.authenticationHandler?() {
            _headers += authHeaders
        }
        
        if let body = self.body {
            
            //-- content type
            if _headers["Content-Type"] == nil {
                _headers["Content-Type"] = self.contentType.rawValue
            }
            
            //-- content length
            _headers["Content-Length"] = "\(body.count)"
        }
        
        var urlRequest = URLRequest(url: self.url)
        
        //-- cell
        urlRequest.allowsCellularAccess = self.configurations.allowCellUsage
        urlRequest.allowsExpensiveNetworkAccess = self.configurations.allowCellUsage
        urlRequest.allowsConstrainedNetworkAccess = self.configurations.allowCellUsage
        
        //-- timeout
        urlRequest.timeoutInterval = self.configurations.timeoutInterval
        
        //-- cache policy
        urlRequest.cachePolicy = self.cachePolicy
        
        //-- headers
        self.configurations.headers = _headers
        urlRequest.allHTTPHeaderFields = _headers
        
        //-- method
        urlRequest.httpMethod = (self.method == .upload ? .post :
                                    (self.method == .download ? .get :
                                        self.method)).rawValue
        
        return urlRequest
    }
    
    internal func createTask(completion: @escaping Request.TaskHandler) -> URLSessionTask? {
        
        var urlRequest = self.createRequest()
        
        //-- upload request
        if self.method == .upload, let data = self.body {
            self.task = self.session.uploadTask(with: urlRequest, from: data)
            
        //-- download request
        } else if self.method == .download {
            self.task = self.session.downloadTask(with: urlRequest)
            self.taskHandler = completion
            
        //-- standard request
        } else {
            if let body = self.body {
                urlRequest.httpBody = body
            }
            
            self.task = self.session.dataTask(with: urlRequest)
            self.taskHandler = completion
        }
        
        return self.task
    }
    
    //-- MARK: request recording (stub creation)
    
    fileprivate func storeRequestInfo() {
        
        //-- if available, use the debug handler instead of writing to a file
        if let handler = self.debugHandler {
            let reqInfo = DebugInfo(url: self.url, body: self.body, headers: self.configurations.headers)
            handler(reqInfo, nil)
            return
        }
        
        guard Target.isDebuggingNetwork else {
            return
        }
        
        if FileManager.default.fileExists(atPath: Network.shared.storePath) == false {
            do { try FileManager.default.createDirectory(at: url, withIntermediateDirectories: true)
            } catch { return }
        }
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy_HH:mm:ss.SSS"
        let filename = "\(Network.shared.storePath)/\(self.url.baseURL?.absoluteString ?? "")_\(dateFormatter.string(from: Date())).log"
        let info: [String:Any] = ["request" : ["url": self.url.absoluteString,
                                               "headers": self.configurations.headers ?? "no headers",
                                               "body": self.body?.utf8String ?? "no body"]]
        
        guard let data = info.jsonData else {
            return
        }
        
        Log.debug(self.methodName(), "storing request @ \(filename)")
        FileManager.default.createFile(atPath: filename, contents: data)
    }
    
    fileprivate func storeResponseInfo(_ response: URLResponse?, responseBody: Data?) {
        guard Target.isDebuggingNetwork || self.debugHandler != nil else {
            return
        }
        
        var headers: [String:Any]? = nil
        if let httpResponse = response as? HTTPURLResponse {
            headers = httpResponse.allHeaderFields as? [String:Any]
        }
        
        //-- if available, use the debug handler instead of writing to a file
        if let handler = self.debugHandler {
            let reqInfo = DebugInfo(url: self.url, body: self.body, headers: self.configurations.headers)
            var resInfo: DebugInfo? = nil
            
            if (response == nil && responseBody == nil) == false {
                resInfo = DebugInfo(url: response?.url, body: responseBody, headers: headers)
            }
            
            handler(reqInfo, resInfo)
            return
        }
        
        if FileManager.default.fileExists(atPath: Network.shared.storePath) == false {
            do {
                try FileManager.default.createDirectory(at: URL(fileURLWithPath: Network.shared.storePath),
                                                        withIntermediateDirectories: true)
            } catch {
                Log.error(ClassInfo.className(self), "Folder not created: \(error)")
                return
            }
        }
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy_HH:mm:ss.SSS"
        let filename = "\(Network.shared.storePath)/\(self.url.baseURL?.absoluteString ?? "")_\(dateFormatter.string(from: Date())).log"
        let info: [String:Any] = ["request" : ["url": self.url.absoluteString,
                                               "headers": self.configurations.headers ?? "no headers",
                                               "body": self.body?.utf8String ?? "no body"],
                                  
                                  "response": ["url": response?.url?.absoluteString ?? "no url",
                                               "headers": headers ?? "no headers",
                                               "body": responseBody?.utf8String ?? "no body"]]
        
        guard let data = info.jsonData else {
            return
        }
        
        Log.debug(self.methodName(), "storing response @ \(filename)")
        FileManager.default.createFile(atPath: filename, contents: data)
    }
    
    //-- MARK: error handling
    
    public static func handleError(_ error: Error, host: String? = nil) -> NetworkError {
        if let error = error as? NetworkError {
            return error
        }
        
        if error._domain == NSPOSIXErrorDomain {
            switch error._code {
            case Int(EPERM):
                return .operationNotPermitted
                
            case Int(ENOSPC):
                return .noSpaceLeftOnDevice
                
            default:
                assert(false, "Unknown error...")
                return .generic("An error occurred trying to reach \(host ?? "the host"). Error: \(error.localizedDescription)", error)
            }
        }
        
        switch error._code {
        case NSURLErrorAppTransportSecurityRequiresSecureConnection:
            return .appTransportSecurityRequiresSecureConnection
            
        case NSURLErrorBackgroundSessionWasDisconnected:
            return .backgroundSessionWasDisconnected
            
        case NSURLErrorDataLengthExceedsMaximum:
            return .dataLengthExceedsMaximum
            
        case NSURLErrorNotConnectedToInternet:
            return .noInternetConnection
            
        case NSURLErrorNetworkConnectionLost:
            return .networkConnectionLost
            
        case NSURLErrorUnsupportedURL:
            return .unsupportedURL
            
        case NSURLErrorCannotFindHost:
            return .cannotFindHost
            
        case NSURLErrorCannotConnectToHost:
            return .cannotConnectToHost
            
        case NSURLErrorCancelled:
            return .cancelled
            
        case NSURLErrorTimedOut:
            return .timeout
            
        default:
            assert(false, "Unknown error...")
            return .generic("An error occurred trying to reach \(host ?? "the host"). Error: \(error.localizedDescription)", error)
        }
    }
    
    open func handleStatusCode(_ status: Int) -> NetworkError {
        switch status {
        case 301: return .movedPermanently
        case 308: return .permanentRedirect
        case 400: return .badRequest
        case 401: return .unauthorized
        case 403: return .forbidden
        case 404: return .notFound
        case 405: return .methodNotAllowed
        case 415: return .unsupportedMediaType
        case 500: return .internalServerError
        case 502: return .badGateway
        case 503: return .serviceUnavailable
        case 504: return .gatewayTimeout
        case 525: return .sslHandshakeFailed
        default:
            assert(false, "Unknown status code \(status)...")
            return .httpError(status)
        }
    }

    //-- MARK: Request: URLSessionDownloadDelegate
    //--
    //-- Note: the URLSession delegates are only considered when there is no completion handler configured on the task
    //--
    
    private var downloadUrl: URL?
    
    public func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didWriteData bytesWritten: Int64, totalBytesWritten: Int64, totalBytesExpectedToWrite: Int64) {
        self.totalTransefered = totalBytesWritten
        self.totalTransferSize = downloadTask.countOfBytesExpectedToReceive
        self.progressHandler?(self.configurations.identifier, self.totalTransefered, self.totalTransferSize)
    }
    
    public func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didFinishDownloadingTo location: URL) {
        let newPath = FileManager.default.urls(for: .cachesDirectory, in: .userDomainMask).first!
        self.downloadUrl = location.moveFile(to: newPath, as: "downloaded.file")
        
        self.totalTransefered = downloadTask.countOfBytesReceived
        self.totalTransferSize = downloadTask.countOfBytesExpectedToReceive
        self.progressHandler?(self.configurations.identifier, self.totalTransefered, self.totalTransferSize)
    }

    //-- MARK: Request: URLSessionDataDelegate
    //--
    //-- Note: the URLSession delegates are only considered when there is no completion handler configured on the task
    //--
    private var responseData: Data?
    
    public func urlSession(_ session: URLSession, task: URLSessionTask, didSendBodyData bytesSent: Int64, totalBytesSent: Int64, totalBytesExpectedToSend: Int64) {
        self.totalTransefered = totalBytesSent
        self.totalTransferSize = totalBytesExpectedToSend
        self.progressHandler?(self.configurations.identifier, self.totalTransefered, self.totalTransferSize)
    }
    
    public func urlSession(_ session: URLSession, dataTask: URLSessionDataTask, didReceive data: Data) {
        if let _ = self.responseData {
            self.responseData?.append(data)
            
        } else {
            self.responseData = data
        }
        
        self.totalTransefered += Int64(data.count)
        self.totalTransferSize = dataTask.countOfBytesExpectedToSend
        self.progressHandler?(self.configurations.identifier, self.totalTransefered, self.totalTransferSize)
    }

    //-- MARK: Request: URLSessionDelegate
    //--
    //-- Note: the URLSession delegates are only considered when there is no completion handler configured on the task
    //--
    
    public func urlSession(_ session: URLSession, task: URLSessionTask, didCompleteWithError error: (any Error)?) {
        self.taskHandler?(self.responseData, task.response, error)
    }
    
    public func urlSession(_ session: URLSession, dataTask: URLSessionDataTask, willCacheResponse proposedResponse: CachedURLResponse, completionHandler: @escaping (CachedURLResponse?) -> Void) {
        completionHandler(proposedResponse)
    }
}
