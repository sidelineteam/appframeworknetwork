//
//  LocalNetwork.swift
//  AppFrameworkNetwork
//
//  Created by Jorge Miguel on 24/02/2025.
//

//*********************************************************************
//
//  Required configurations
//  - Add the items "_bonjour._tcp" and "_lnp._tcp." to the
//      NSBonjourServices key in the project's info.plist
//
//  - Add a description to the NSLocalNetworkUsageDescription key
//      in the project's info.plist
//
//*********************************************************************

#if os(iOS)

import Network
import Foundation
import AppFrameworkCore
import MultipeerConnectivity

public class LocalNetwork: NSObject {
    
    private var browser: NWBrowser?
    private var netService: NetService?
    private var completion: ((Bool) -> Void)?
    
    private let serviceType = "app"
    private let peerId = MCPeerID(displayName: UIDevice.current.name)
    private var serviceAdvertiser: MCNearbyServiceAdvertiser?
    private var serviceBrowser: MCNearbyServiceBrowser?
    private var session: MCSession?
    
    public func requestAuthorization(completion: @escaping (Bool) -> Void) {
        self.completion = completion
        
        // Create parameters, and allow browsing over peer-to-peer link.
        let parameters = NWParameters()
        parameters.includePeerToPeer = true
        
        // Browse for a custom service type.
        let browser = NWBrowser(for: .bonjour(type: "_bonjour._tcp", domain: nil), using: parameters)
        self.browser = browser
        browser.stateUpdateHandler = { newState in
            switch newState {
            case .failed(let error):
                Log.debug(ClassInfo.methodName(self), "🛜 Local network permission: \(error.localizedDescription)")
            case .ready, .cancelled:
                break
            case let .waiting(error):
                Log.debug(ClassInfo.methodName(self), "🛜 Local network permission: is pending or has been denied: \(error)")
                self.requestAuthorization()
                
                self.reset()
                self.completion?(false)
            default:
                break
            }
        }
        
        self.netService = NetService(domain: "local.", type:"_lnp._tcp.", name: "LocalNetworkPrivacy", port: 1100)
        self.netService?.delegate = self
        
        self.browser?.start(queue: .main)
        self.netService?.publish()
    }
    
    public func requestAuthorization() {
        session = MCSession(peer: peerId, securityIdentity: nil, encryptionPreference: .none)
        serviceAdvertiser = MCNearbyServiceAdvertiser(peer: peerId, discoveryInfo: ["event" : "hello"], serviceType: serviceType)
        serviceBrowser = MCNearbyServiceBrowser(peer: peerId, serviceType: serviceType)
        
        serviceAdvertiser?.startAdvertisingPeer()
        serviceBrowser?.startBrowsingForPeers()
    }
    
    private func reset() {
        self.browser?.cancel()
        self.browser = nil
        self.netService?.stop()
        self.netService = nil
    }
    
    deinit {
        serviceAdvertiser?.stopAdvertisingPeer()
        serviceBrowser?.stopBrowsingForPeers()
    }
}

extension LocalNetwork : NetServiceDelegate {
    
    public func netServiceDidPublish(_ sender: NetService) {
        self.reset()
        
        Log.debug(ClassInfo.methodName(self), "🛜 Local network permission: has been granted")
        completion?(true)
    }
}

#endif
