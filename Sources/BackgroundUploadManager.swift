//
//  BackgroundUploadManager.swift
//  AppFrameworkV2
//
//  Created by Jorge Miguel on 20/10/2018.
//  Copyright (c) 2018 Jorge Miguel. All rights reserved.
//

import Foundation
import AppFrameworkCore

public enum BackgroundUploadInfo : String {
    case remoteId = "remoteId"
    case path     = "path"
    case startAt  = "startAt"
    case ownerId  = "ownerId"
}

//--

public protocol BackgroundUploadManagerDelegate : AnyObject {

    func backgroundUpload(task: URLSessionTask, authChallenge: URLAuthenticationChallenge, taskInfo: [BackgroundUploadInfo : String]) -> URLCredential?

    func backgroundUpload(task: URLSessionTask, progress: Int, taskInfo: [BackgroundUploadInfo : String])

    func backgroundUpload(task: URLSessionTask, responseData: Data?, error: Error?, taskInfo: [BackgroundUploadInfo : String])

}

//--

public class BackgroundUploadManager : NSObject {

    public static let shared = BackgroundUploadManager()

    public weak var delegate: BackgroundUploadManagerDelegate?

    //-- setup on AppDelegate's func application(_ application:, handleEventsForBackgroundURLSession identifier:, completionHandler:)
    public var backgroundCompletionHandler: (() -> Void)?

    private var responses = [Int: Data]()
}

extension BackgroundUploadManager : URLSessionDelegate {

    public func urlSession(_ session: URLSession, didBecomeInvalidWithError error: Error?) {
        Log.background(self.methodName(), "Error: \((error?.localizedDescription) ?? "")", level: .error)
    }

    public func urlSessionDidFinishEvents(forBackgroundURLSession session: URLSession) {
        Log.background(self.methodName(), "DidFinishEvents", level: .notice)
    }

}

extension BackgroundUploadManager : URLSessionTaskDelegate {

    public func urlSession(_ session: URLSession, task: URLSessionTask, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {
        Log.background(self.methodName(), "Session did receive auth challenge", level: .notice)

        let credential = self.delegate?.backgroundUpload(task: task, authChallenge: challenge, taskInfo: self.taskInfo(task.taskDescription))

        completionHandler(.useCredential, credential)
    }

    public func urlSession(_ session: URLSession, task: URLSessionTask, needNewBodyStream completionHandler: @escaping (InputStream?) -> Void) {
        Log.notice(self.methodName(), "Session \(session.sessionDescription ?? "unknown")")

        let info = taskInfo(task.taskDescription)
        guard info.count >= 4 else {
            completionHandler(nil)
            return
        }

        //-- already sent by the task
        let sent =  UInt64(task.countOfBytesSent)

        //-- resumable API
        guard let path = info[.path],
              let sstartAt = info[.startAt],
              let startAt = UInt64(sstartAt) else {
            return
        }

        guard FileManager.default.fileExists(atPath: path) else {
            Log.background(self.methodName(), "File doesn't exists at path \(path)", level: .error)
            return
        }

        guard let handler = FileHandle(forReadingAtPath: path) else {
            Log.background(self.methodName(), "Error creating handler for path \(path)", level: .error)
            return
        }

        let seek = startAt + sent
        if seek > 0 {
            Log.background(self.methodName(), "Data will seek to byte \(seek)")
            handler.seek(toFileOffset: seek)
        }

        let stream = InputStream(data: handler.readDataToEndOfFile())

        completionHandler(stream)
    }

    public func urlSession(_ session: URLSession, task: URLSessionTask, didSendBodyData bytesSent: Int64, totalBytesSent: Int64, totalBytesExpectedToSend: Int64) {
        let percent = Double(totalBytesSent) / Double(totalBytesExpectedToSend) * 100

        let info = taskInfo(task.taskDescription)
        guard info.count == 3 else {
            return
        }

        self.delegate?.backgroundUpload(task: task, progress: Int(percent), taskInfo: info)
    }

    public func urlSession(_ session: URLSession, task: URLSessionTask, didCompleteWithError error: Error?) {
        Log.background(self.methodName(),
                       "Task: \(task.taskIdentifier) completed \(error != nil ? "with error \(error!)" : "successfully").",
                       level: (error != nil ? .error : .info))

        let info = taskInfo(task.taskDescription)
        guard info.count >= 4 else {
            Log.error(methodName(), "TaskInfo is not complete!")
            self.responses.removeValue(forKey: task.taskIdentifier)
            return
        }

        //-- handle finished task
        self.delegate?.backgroundUpload(task: task, responseData: self.responses[task.taskIdentifier], error: error, taskInfo: info)

        //-- complete background session
        self.backgroundCompletionHandler?()

        //-- clean up response data
        self.responses.removeValue(forKey: task.taskIdentifier)
    }

    //-- MARK: helpers

    public static func storeInfo(remoteId: String = "", path: String = "", startAt: String = "", ownerId: String = "") -> String {
        return "\(remoteId)|\(path)|\(startAt)|\(ownerId)"
    }

    private func taskInfo(_ info: String?) -> [BackgroundUploadInfo : String] {
        guard let info = info?.components(separatedBy: "|"), info.count >= 4 else {
            return [:]
        }

        return [.remoteId: info[0],
                .path:     info[1],
                .startAt:  info[2],
                .ownerId:  info[3]]
    }

}

extension BackgroundUploadManager : URLSessionDataDelegate {

    public func urlSession(_ session: URLSession, dataTask: URLSessionDataTask, didReceive data: Data) {
        self.responses[dataTask.taskIdentifier] = data
    }

}
