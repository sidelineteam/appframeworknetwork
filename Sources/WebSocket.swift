//
//  WebSocketRequest.swift
//  AppFramework
//
//  Created by Jorge Miguel on 10/04/2021.
//  Copyright (c) 2021 Jorge Miguel. All rights reserved.
//

//*********************************************************************
// -network-debug launch argument can be used to enable the debug mode
//*********************************************************************

import Foundation
import AppFrameworkCore

@available(iOS 13, *)
public extension WebSocket {
    
    enum Status {
        
        case open
        case closed
    }

    typealias Message = URLSessionWebSocketTask.Message
    
    typealias SenderHandler = (_ error: Error?) -> Void
    typealias StatusHandler = (_ status: WebSocket.Status) -> Void
    typealias DebugHandler  = (_ message: WebSocket.Message) -> Void
    typealias ReceiverHandler = (_ message: WebSocket.Message?, _ error: Error?) -> Void
}

//--

@available(iOS 13, *)
public class WebSocket : NSObject {
    
    private var retryDelay: DispatchTime { .now() + 5 }
    
    private(set) var status: WebSocket.Status = .closed { didSet { self.statusHandler?(self.status) } }
    private var task: URLSessionWebSocketTask?
    
    private let debugHandler: WebSocket.DebugHandler?
    private let statusHandler: WebSocket.StatusHandler?
    private let receiverHandler: WebSocket.ReceiverHandler?
    
    private let url: URL
    
    private lazy var session: URLSession = { URLSession(configuration: NetworkSessions.defaultConfiguration, delegate: self, delegateQueue: nil) }()
    
    private typealias ReceiveHandler = (Result<URLSessionWebSocketTask.Message, Error>) -> Void
    private lazy var receiver: ReceiveHandler = { result in
        
        switch result {
        case .failure(let error):
            DispatchQueue.global().asyncAfter(deadline: self.retryDelay) {
                //-- register receiver... again
                //self.task?.receive(completionHandler: self.receiver)
                self.reset()
            }
            
            self.receiverHandler?(nil, error)
            
        case .success(let message):
            //-- register receiver... again
            self.task?.receive(completionHandler: self.receiver)
            self.receiverHandler?(message, nil)
            self.storeMessage(message)
        }
    }
    
    //--
    
    public init(url: URL, status: WebSocket.StatusHandler? = nil, receiver: WebSocket.ReceiverHandler? = nil, debug: WebSocket.DebugHandler? = nil) {
        self.receiverHandler = receiver
        self.statusHandler = status
        self.debugHandler = debug
        self.url = url
    }
    
    private func setupTask() {
        let request = URLRequest(url: self.url)
        self.task = self.session.webSocketTask(with: request)
        self.task?.receive(completionHandler: self.receiver)
    }
    
    //-- MARK: life cycle
    
    public func start() {
        self.setupTask()
        self.task?.resume()
    }
    
    public func pause() {
        self.task?.suspend()
    }
    
    public func stop() {
        self.task?.cancel(with: .goingAway, reason: nil)
        self.task = nil
    }
    
    public func reset() {
        self.stop()
        
        DispatchQueue.global().asyncAfter(deadline: self.retryDelay) {
            self.start()
        }
    }
    
    //-- MARK: message
    
    public func send(_ message: String, completion: @escaping WebSocket.SenderHandler) {
        self.task?.send(Message.string(message), completionHandler: completion)
    }
    
    public func send(_ message: Data, completion: @escaping WebSocket.SenderHandler) {
        self.task?.send(Message.data(message), completionHandler: completion)
    }
    
    //-- MARK: message recording (stub creation)
    
    private func storeMessage(_ message: WebSocket.Message) {
        guard Target.isDebuggingNetwork else {
            return
        }
        
        //-- if available, use the debug handler instead of writing to a file
        if let handler = self.debugHandler {
            handler(message)
            return
        }
        
        if FileManager.default.fileExists(atPath: Network.shared.storePath) == false {
            do { try FileManager.default.createDirectory(at: url, withIntermediateDirectories: true)
            } catch { return }
        }
        
        let filename = "\(Network.shared.storePath)/\(self.url.baseURL?.absoluteString ?? "")_\(Date().isoDate).log"
        let info: [String:Any] = ["request" : ["url": self.url.absoluteString, "message": message]]
            
        guard let data = info.jsonData else {
            return
        }
        
        Log.debug(self.methodName(), "storing request @ \(filename)")
        FileManager.default.createFile(atPath: filename, contents: data)
    }
}

@available(iOS 13.0, *)
extension WebSocket : URLSessionWebSocketDelegate {
    
    public func urlSession(_ session: URLSession, webSocketTask: URLSessionWebSocketTask, didOpenWithProtocol protocol: String?) {
        self.status = .open
    }
    
    public func urlSession(_ session: URLSession, webSocketTask: URLSessionWebSocketTask, didCloseWith closeCode: URLSessionWebSocketTask.CloseCode, reason: Data?) {
        self.status = .closed
    }
}
