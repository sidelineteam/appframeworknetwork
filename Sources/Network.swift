//
//  Network.swift
//  AppFrameworkV2
//
//  Created by Jorge Miguel on 06/07/18.
//  Copyright (c) 2018 Jorge Miguel. All rights reserved.
//

#if os(iOS)
import Reachability
#endif

import UIKit
import Foundation
import AppFrameworkCore

public typealias IdleHandler = () -> Void

//---------------------------------------

public enum NetworkError: Error, Equatable {
    
    case generic(String, Error?)

    //-- URLDomain
    case backgroundSessionWasDisconnected               //-- (-997)  the background session was disconnected
    case cancelled                                      //-- (-999)  the request was cancelled
    case timeout                                        //-- (-1001) the request timed out
    case unsupportedURL                                 //-- (-1002) unsupported URL
    case cannotFindHost                                 //-- (-1003) cannot find the host
    case cannotConnectToHost                            //-- (-1004) could not connect with server
    case networkConnectionLost                          //-- (-1005) network connection lost
    case noInternetConnection                           //-- (-1009) the client is offline
    case appTransportSecurityRequiresSecureConnection   //-- (-1022) https required
    case dataLengthExceedsMaximum                       //-- (-1103) resource exceeds maximum length
    case errorSecureConnectionFailed                    //-- (-1200) an SSL error has occurred and a secure connection to the server cannot be made.

    //-- NSPOSIXErrorDomain
    case operationNotPermitted                          //-- operation not permitted (EPERM = 1)
    case noSpaceLeftOnDevice                            //-- no space left on device (ENOSPC = 28)

    //-- HTTP
    case httpError(Int)                                 //-- unparsed http error
    case movedPermanently                               //-- the request URL was permanently moved to another location (301)
    case permanentRedirect                              //-- the request was permanently redirected (308)
    case badRequest                                     //-- the request was not computed successfully by the server (400)
    case unauthorized                                   //-- the request was not authorized (401)
    case forbidden                                      //-- the request is forbidden (403)
    case notFound                                       //-- the requested endpoint was not found (404)
    case methodNotAllowed                               //-- the request method is not allowed (405)
    case unsupportedMediaType                           //-- the requests media type is not supported (415)
    case internalServerError                            //-- internal server error (500)
    case notImplemented                                 //-- not implemented (501)
    case badGateway                                     //-- the requested endpoint was not found (502)
    case serviceUnavailable                             //-- server is unavailable (503)
    case gatewayTimeout                                 //-- gateway timeout (504)
    case sslHandshakeFailed                             //-- ssl handshake failed (525)

    case badResponse                                    //-- the response couldn't be parsed
    case wrongOrMissingCallaback                        //-- there is no success callbak available or is of the wrong type

    public var localizedDescription : String { String(reflecting: self) }
    
    public static func == (lhs: NetworkError, rhs: NetworkError) -> Bool { lhs.code == rhs.code }
    
    public var code: Int {
        switch self {
        //-- URLDomain
        case .backgroundSessionWasDisconnected:             return -997
        case .cancelled:                                    return -999
        case .timeout:                                      return -1001
        case .unsupportedURL:                               return -1002
        case .cannotFindHost:                               return -1003
        case .networkConnectionLost:                        return -1005
        case .noInternetConnection:                         return -1009
        case .appTransportSecurityRequiresSecureConnection: return -1022
        case .dataLengthExceedsMaximum:                     return -1103
        case .errorSecureConnectionFailed:                  return -1200

        //-- NSPOSIXErrorDomain
        case .operationNotPermitted: return 1
        case .noSpaceLeftOnDevice:   return 28

        //-- HTTP
        case .httpError(let code):  return code
        case .movedPermanently:     return 301
        case .permanentRedirect:    return 308
        case .badRequest:           return 400
        case .unauthorized:         return 401
        case .forbidden:            return 403
        case .notFound:             return 404
        case .methodNotAllowed:     return 405
        case .unsupportedMediaType: return 415
        case .internalServerError:  return 500
        case .notImplemented:       return 501
        case .badGateway:           return 502
        case .serviceUnavailable:   return 503

        default: return -1
        }
    }
}

//---------------------------------------

public struct Network {

    public static var shared = Network()
    
    internal lazy var storePath: String = {
        let path = "\(NSTemporaryDirectory())requests"
        Log.notice(ClassInfo.methodName(self), "Request's records path: \(path)")
        return path
    }()
    
    public var sessionProtocolClasses: [AnyClass]? {
        didSet {
            NetworkSessions.backgroundConfiguration.protocolClasses = self.sessionProtocolClasses
            NetworkSessions.defaultConfiguration.protocolClasses = self.sessionProtocolClasses
        }
    }

    private var queues = [String:OperationQueue]()
    
    //-- MARK: initializer

#if !os(watchOS)
    init() {
        _ = self.reachability
    }
#endif

    //-- MARK: network indicator

    public static var enableNetworkIndicator: Bool = true {
        didSet(newValue) {
            #if !kExtensionTarget
            if newValue == false {
                self.shared.networkActivityCounter = 0
//                if enableNetworkIndicator != newValue {
//                    DispatchQueue.main.async {
//                        //UIApplication.shared.isNetworkActivityIndicatorVisible = false
//                    }
//                }
            }
            #endif
        }
    }

    private var networkActivityCounter: Int = 0

    public static func showNetworkActivity(_ show: Bool) {
        #if !kExtensionTarget
        guard enableNetworkIndicator else {
            return
        }

        guard self.shared.networkActivityCounter > 0 || show else {
            return
        }

        self.shared.networkActivityCounter += (show ? 1 : -1)

        DispatchQueue.main.async {
            if self.shared.networkActivityCounter == 1 {
                //UIApplication.shared.isNetworkActivityIndicatorVisible = true

            } else if self.shared.networkActivityCounter == 0 {
                //UIApplication.shared.isNetworkActivityIndicatorVisible = false
            }
        }
        #endif
    }

    //-- MARK: queues

    public static func setupQueue(_ name: String, maxConcurrency: Int = 1, qualityOfService: QualityOfService = .default) {
        guard name.count > 0 else {
            Log.warning(ClassInfo.methodName(self), "Invalid queue name!")
            return
        }

        var queue = self.shared.queues[name]
        if queue == nil {
            queue = OperationQueue()
            self.shared.queues[name] = queue
        }

        queue?.qualityOfService = qualityOfService
        queue?.maxConcurrentOperationCount = maxConcurrency
    }

    @discardableResult
    public static func add<T>(request: Request<T>, toQueue queue: String? = nil, priority: Operation.QueuePriority = .normal) -> String {
        request.queuePriority = priority
        
        let queueName = queue ?? request.url.host ?? "default"
        guard let queue = self.shared.queues[queueName] else {
            let newQueue = OperationQueue()
            newQueue.maxConcurrentOperationCount = StaticConfigs.Network.maxConcurrentRequestsPerHost
            newQueue.addOperation(request)
            
            self.shared.queues[queueName] = newQueue
            
            return request.configurations.identifier
        }

        queue.addOperation(request)
        
        if priority.rawValue > Operation.QueuePriority.normal.rawValue {
            queue.isSuspended = false
        }
        
        return request.configurations.identifier
    }
    
    public static func requests(on queue: String) -> [Request<Any>] {
        return self.shared.queues[queue]?.operations as! [Request<Any>]
    }

    public static func cancelRequest(_ identifier: String) {
        self.shared.queues.values.forEach { queue in
            queue.operations.forEach({ request in
                guard let request = request as? Request<Any> else {
                    return
                }

                if request.configurations.identifier == identifier {
                    request.cancel()
                }
            })
        }
    }
    
    public static func cancelRequests(on queue: String) {
        self.shared.queues[queue]?.cancelAllOperations()
        self.shared.queues.removeValue(forKey: queue)
    }

    public static func cancelAllRequests() {
        self.shared.queues.values.forEach { queue in
            queue.cancelAllOperations()
            queue.isSuspended = false
        }
    }

    public static func suspendAllQueues() {
        self.shared.queues.values.forEach { queue in
            queue.isSuspended = true
        }
    }
    
    public static func suspendRequests(on queue: String) {
        self.shared.queues[queue]?.isSuspended = true
    }

    public static func resumeAllQueues() {
        self.shared.queues.values.forEach { queue in
            queue.isSuspended = false
        }
    }
    
    public static func resumeRequests(on queue: String) {
        self.shared.queues[queue]?.isSuspended = false
    }

    public static func onIdle(_ handler: @escaping IdleHandler) {
        DispatchQueue.global().async {

            //-- waits for all the queues to finish
            self.shared.queues.values.forEach({ queue in
                queue.waitUntilAllOperationsAreFinished()
            })

            handler()
        }
    }

    //-- MARK: cache

    public static func resetAllCache() {
        URLCache.shared.removeAllCachedResponses()
    }
    
    //-- MARK: reachability

#if !os(watchOS)
    
    public static var isOnWifi: Bool { self.shared.reachability?.connection == .wifi }
    public static var isOnCell: Bool { self.shared.reachability?.connection == .cellular }
    public static var isActive: Bool { self.shared.reachability?.connection != .unavailable }

    private let reachability: Reachability? = {
        guard let reachability = try? Reachability() else {
            Log.error("Failed to run Reachability! App will assume offline mode.")
            return nil
        }
        
        return reachability
    }()

#else
    
    public static var isOnWifi: Bool { true }
    public static var isOnCell: Bool { true }
    public static var isActive: Bool { true }
    
#endif

}
